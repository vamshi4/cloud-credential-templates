# terraform init
terraform {
  backend "s3" {
    region = "us-east-1"
    encrypt = true
  }
  required_providers {
    volterra = {
      source = "volterraedge/volterra"
    }
  }
}

# variables
variable "gcp_credentials_base64" {}
variable "gcp_zone" {}
variable "gcp_region" {}
variable "gcp_project_id" {}
variable "gcp_sa_name" {}
variable "name" {}
variable "f5xc_name" {}
variable "ssh_key" {
  default = ""
}

variable "api_p12_file" {}
variable "api_url" {}

# providers
provider "google" {
  project     = var.gcp_project_id
  region      = var.gcp_region
  zone        = var.gcp_zone
  credentials = base64decode(var.gcp_credentials_base64)
}

provider "volterra" {
  api_p12_file = var.api_p12_file
  url          = var.api_url
}

# local variables
locals {
  permissions                 = yamldecode(file("${path.module}/../../gcp/f5xc_gcp_vpc_role.yaml"))["includedPermissions"]
  role_id                     = google_project_iam_custom_role.gcp_role.role_id
  role_binding                = "projects/${var.gcp_project_id}/roles/${local.role_id}"
  service_account_email       = google_service_account.service_account.email
  member                      = "serviceAccount:${local.service_account_email}"
  service_account_key_base64  = google_service_account_key.service_account_key.private_key
}

# resources
## generate random string for gcp custom role
resource "random_string" "name" {
  length  = 2
  special = false
  lower   = true
}

## create gcp custom role
resource "google_project_iam_custom_role" "gcp_role" {
  role_id       = format("%s_%s", var.name, random_string.name.result)
  title         = format("%s_%s", var.name, random_string.name.result)
  description   = "GCP role to create F5XC GCP VPC site"
  permissions   = local.permissions
}

## create gcp service account
resource "google_service_account" "service_account" {
  depends_on   = [ google_project_iam_custom_role.gcp_role ]
  account_id   = var.gcp_sa_name
  display_name = var.gcp_sa_name
  project      = var.gcp_project_id
}

## bind gcp service account with role in the project
resource "google_project_iam_binding" "binding" {
  depends_on   = [ google_service_account.service_account ]
  project      = var.gcp_project_id
  role         = local.role_binding
  members      = [ local.member ]
}

## create gcp service account key
resource "google_service_account_key" "service_account_key" {
  depends_on         = [ google_project_iam_binding.binding ]
  service_account_id = google_service_account.service_account.name
}

## create volterra gcp credentials
resource "volterra_cloud_credentials" "gcp_creds" {
  depends_on  = [ google_service_account_key.service_account_key ]
  name        = format("%s-creds", var.f5xc_name)
  namespace   = "system"
  description = "f5xc azure vnet site for cloud-credentials"
  gcp_cred_file {
    credential_file {
      clear_secret_info {
        url = "string:///${local.service_account_key_base64}"
      }
    }
  }
}

## create volterra gcp vpc site
resource "volterra_gcp_vpc_site" "site" {
  name        = var.f5xc_name
  namespace   = "system"
  description = "f5xc gcp vpc site for cloud-credentials"
  gcp_region  = var.gcp_region

  cloud_credentials {
    name      = volterra_cloud_credentials.gcp_creds.name
    namespace = "system"
  }

  instance_type           = "n1-standard-8"
  disk_size               = 40
  ssh_key                 = var.ssh_key
  logs_streaming_disabled = true
  nodes_per_az            = 0

  ingress_egress_gw {
    gcp_certified_hw = "gcp-byol-multi-nic-voltmesh"
    node_number      = 3
    gcp_zone_names   = [format("%s-a", var.gcp_region), format("%s-b", var.gcp_region), format("%s-c", var.gcp_region)]
    inside_network {
      new_network_autogenerate {
        autogenerate = true
      }
    }
    inside_subnet {
      new_subnet {
        primary_ipv4 = "192.168.0.0/24"
        subnet_name  = "${var.f5xc_name}-inside-subnet"
      }
    }

    outside_network {
      new_network_autogenerate {
        autogenerate = true
      }
    }
    outside_subnet {
      new_subnet {
        primary_ipv4 = "192.168.1.0/24"
        subnet_name  = "${var.f5xc_name}-outside-subnet"
      }
    }

    no_network_policy        = true
    no_forward_proxy         = true
    no_global_network        = true
    no_inside_static_routes  = true
    no_outside_static_routes = true

    performance_enhancement_mode {
      perf_mode_l7_enhanced = true
    }
  }

  offline_survivability_mode {
    no_offline_survivability_mode = true
  }

  lifecycle {
    ignore_changes = [labels, description]
  }
}

## get status of volterra gcp vpc site
resource "volterra_tf_params_action" "apply_site" {
  depends_on      = [
    volterra_gcp_vpc_site.site,
    google_service_account_key.service_account_key,
    google_project_iam_custom_role.gcp_role
  ]
  site_name       = var.f5xc_name
  site_kind       = "gcp_vpc_site"
  action          = "apply"
  wait_for_action = true
}

# outputs
output "volterra_tf_params_action" {
  value = volterra_tf_params_action.apply_site
}