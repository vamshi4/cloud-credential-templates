#!/bin/bash

# get script base directory
BASEDIR=$(dirname "$0")

# get name from tfvars.json
F5XC_NAME=$(jq .f5xc_name "${BASEDIR}/tfvars.json" | tr -d \")
NAME=${F5XC_NAME:-f5xc-cc-gcp-vpc-site}

# create gcp credentials file
echo "${GOOGLE_APPLICATION_CREDENTIALS_BASE64}" | base64 -d > /tmp/key.json

# login to gcp
echo "logging into gcloud.."
gcloud auth activate-service-account --key-file=/tmp/key.json --no-user-output-enabled

# lookout for resources
ROUTES=$(gcloud compute routes list --filter="name:( "${NAME}-inside" )" | grep "${NAME}" | awk '{print $1}')
FRWD_RULES=$(gcloud compute forwarding-rules list --filter="name:( "${NAME}-inside-forwarding-rule" )" | grep "${NAME}" | awk '{print $1}')
BKND_SVC=$(gcloud compute backend-services list --filter="name:( "${NAME}-inside-backend" )" | grep "${NAME}" | awk '{print $1}')
INST_GRP=$(gcloud compute instance-groups managed list --filter="name:( "${NAME}" )" | grep "${NAME}" | awk '{print $1}')
VM_TMPL=$(gcloud compute instance-templates list | grep "${NAME}" | awk '{print $1}')
HC=$(gcloud compute health-checks list | grep "${NAME}" | awk '{print $1}')
IN_SUBNET=$(gcloud compute networks subnets list | grep "${NAME}-inside" | awk '{print $1}')
OUT_SUBNET=$(gcloud compute networks subnets list | grep "${NAME}-outside" | awk '{print $1}')
IN_NETWORK=$(gcloud compute networks list | grep "${NAME}-inside" | awk '{print $1}')
IN_NETWORK_EGRESS=$(gcloud compute firewall-rules list --filter="network:( "${IN_NETWORK}")" | grep egress | awk '{print $1}')
IN_NETWORK_INGRESS=$(gcloud compute firewall-rules list --filter="network:( "${IN_NETWORK}")" | grep ingress | awk '{print $1}')
OUT_NETWORK=$(gcloud compute networks list | grep "${NAME}-outside" | awk '{print $1}')
OUT_NETWORK_EGRESS=$(gcloud compute firewall-rules list --filter="network:( "${OUT_NETWORK}")" | grep egress | awk '{print $1}')
OUT_NETWORK_INGRESS=$(gcloud compute firewall-rules list --filter="network:( "${OUT_NETWORK}")" | grep ingress | awk '{print $1}')

# delete resources if exist
echo "deleting gcp resources if exist.."

if [ -n "${ROUTES}" ]; then
  gcloud compute routes delete "${ROUTES}" --quiet
fi

if [ -n "${FRWD_RULES}" ]; then
  gcloud compute forwarding-rules delete "${FRWD_RULES}" --region="${GCP_REGION}" --quiet
fi 

if [ -n "${BKND_SVC}" ]; then
  gcloud compute backend-services delete "${BKND_SVC}" --region="${GCP_REGION}" --quiet
fi

if [ -n "${INST_GRP}" ]; then 
  gcloud compute instance-groups managed delete "${INST_GRP}" --region="${GCP_REGION}" --quiet
fi

if [ -n "${VM_TMPL}" ]; then
  gcloud compute instance-templates delete "${VM_TMPL}" --quiet
fi

if [ -n "${HC}" ]; then
  gcloud compute health-checks delete "${HC}" --quiet
fi

if [ -n "${IN_SUBNET}" ]; then
  gcloud compute networks subnets delete "${IN_SUBNET}" --region="${GCP_REGION}" --quiet
fi

if [ -n "${OUT_SUBNET}" ]; then
  gcloud compute networks subnets delete "${OUT_SUBNET}" --region="${GCP_REGION}" --quiet
fi

if [ -n "${IN_NETWORK_EGRESS}" ]; then
  gcloud compute firewall-rules delete "${IN_NETWORK_EGRESS}" --quiet
fi

if [ -n "${IN_NETWORK_INGRESS}" ]; then
  gcloud compute firewall-rules delete "${IN_NETWORK_INGRESS}" --quiet
fi

if [ -n "${OUT_NETWORK_EGRESS}" ]; then
  gcloud compute firewall-rules delete "${OUT_NETWORK_EGRESS}" --quiet
fi

if [ -n "${OUT_NETWORK_INGRESS}" ]; then
  gcloud compute firewall-rules delete "${OUT_NETWORK_INGRESS}" --quiet
fi

if [ -n "${IN_NETWORK}" ]; then
  gcloud compute networks delete "${IN_NETWORK}" --quiet
fi

if [ -n "${OUT_NETWORK}" ]; then
  gcloud compute networks delete "${OUT_NETWORK}" --quiet
fi

echo "gcp resources are deleted/non-existent.."
